function calculateGrade() {
            var mark = document.getElementById("mark").value;
            var grade;

            if (mark >= 90) {
                grade = "A+";
            } else if (mark >= 80) {
                grade = "A";
            } else if (mark >= 70) {
                grade = "B+";
            } else if (mark >= 60) {
                grade = "B";
            } else if (mark >= 50) {
                grade = "C+";
            } else {
                grade = "Failed";
            }

            document.getElementById("grade").textContent = "Grade: " + grade;
        }